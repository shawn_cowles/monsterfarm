﻿using System;
using System.Linq;
using MonsterFarm.Interfaces;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace MonsterFarm
{
    [TestFixture]
    public class FarmTest
    {
        private readonly Fixture _fixture = new Fixture();

        private Farm<TestCreature> _farm;

        private Mock<IDataInitializer<TestCreature>> _mockDataInitializer;
        private Mock<IDataRepository<TestCreature>> _mockDataRepository;
        private Mock<IEvaluator<TestCreature>> _mockEvaluator;
        private Mock<ISettingsProvider> _mockSettingsProvider;
        private Mock<IMutator<TestCreature>> _mockMutator;
        private Mock<ILogger> _mockLogger;

        [SetUp]
        public void Setup()
        {
            _mockDataInitializer = new Mock<IDataInitializer<TestCreature>>();
            _mockDataRepository = new Mock<IDataRepository<TestCreature>>();
            _mockEvaluator = new Mock<IEvaluator<TestCreature>>();
            _mockSettingsProvider = new Mock<ISettingsProvider>();
            _mockMutator = new Mock<IMutator<TestCreature>>();
            _mockLogger = new Mock<ILogger>();

            _farm = new Farm<TestCreature>(
                _mockDataInitializer.Object,
                _mockDataRepository.Object,
                _mockEvaluator.Object,
                _mockSettingsProvider.Object,
                _mockMutator.Object,
                _mockLogger.Object);
        }

        [Test]
        public void Run_calls_Initialize_on_the_data_iniatializer()
        {
            _farm.Run();

            _mockDataInitializer.Verify(x => x.Initialize());
        }

        [Test]
        public void Run_evaluates_each_creature_once_for_each_generation()
        {
            var numGenerations = _fixture.CreateMany<string>().Count();

            var creatures = _fixture.CreateMany<TestCreature>();

            _mockDataRepository
                .Setup(x => x.GetCreatures())
                .Returns(creatures);
            _mockSettingsProvider
                .Setup(x => x.GenerationsToRun)
                .Returns(numGenerations);
            _mockSettingsProvider
                .Setup(x => x.NumberOfCreatures)
                .Returns(creatures.Count());

            _farm.Run();

            foreach(var creature in creatures)
            {
                _mockEvaluator.Verify(x => x.EvaluateCreature(creature),
                    Times.Exactly(numGenerations));
            }
        }

        [Test]
        public void Run_calls_mutate_each_generation_except_the_last()
        {
            var numGenerations = _fixture.CreateMany<string>().Count();

            _mockSettingsProvider
                .Setup(x => x.GenerationsToRun)
                .Returns(numGenerations);

            _farm.Run();

            _mockMutator.Verify(x => x.Mutate(), Times.Exactly(numGenerations - 1));
        }

        [Test]
        public void Run_stores_each_creatures_score_in_the_data_repository()
        {
            var numGenerations = _fixture.CreateMany<string>().Count();

            var creatures = _fixture.CreateMany<TestCreature>();

            var scores = creatures.ToDictionary(x => x, x => _fixture.Create<double>());

            _mockDataRepository
                .Setup(x => x.GetCreatures())
                .Returns(creatures);
            _mockSettingsProvider
                .Setup(x => x.GenerationsToRun)
                .Returns(numGenerations);
            _mockSettingsProvider
                .Setup(x => x.NumberOfCreatures)
                .Returns(creatures.Count());
            _mockEvaluator
                .Setup(x => x.EvaluateCreature(It.IsAny<TestCreature>()))
                .Returns<TestCreature>(creature => scores[creature]);

            _farm.Run();

            foreach (var creature in creatures)
            {
                _mockDataRepository.Verify(x => x.RecordScore(creature, scores[creature]));
            }
        }

        [Test]
        public void Run_reports_status_to_the_logger()
        {
            _farm.Run();

            _mockLogger.Verify(x => x.ReportStatus(It.IsAny<string>()));
        }

        [Test]
        public void Run_reports_progress_to_the_logger()
        {
            var numGenerations = _fixture.CreateMany<string>().Count();

            var creatures = _fixture.CreateMany<TestCreature>();

            _mockDataRepository
                .Setup(x => x.GetCreatures())
                .Returns(creatures);
            _mockSettingsProvider
                .Setup(x => x.GenerationsToRun)
                .Returns(numGenerations);
            _mockSettingsProvider
                .Setup(x => x.NumberOfCreatures)
                .Returns(creatures.Count());
            
            _farm.Run();

            _mockLogger.Verify(x => x.ReportProgress(It.IsAny<int>()));
        }
    }
}
