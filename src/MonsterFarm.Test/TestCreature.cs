﻿using System;
using MonsterFarm.Data;

namespace MonsterFarm
{
    public class TestCreature : ICreature
    {
        public bool WasMutated { get; private set; }

        public Guid Id { get; set; }

        public TestCreature()
        {
            WasMutated = false;
        }
        
        public ICreature Mutate(Random rnd)
        {
            WasMutated = true;

            return this;
        }
    }
}
