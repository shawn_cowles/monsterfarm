﻿using System;
using System.Collections.Generic;
using System.Linq;
using MonsterFarm.Data;
using MonsterFarm.Interfaces;
using Moq;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace MonsterFarm.Impl
{
    public class TopPercentMutatorTest
    {
        private readonly Fixture _fixture = new Fixture();

        public TopPercentMutator<TestCreature> _mutator;

        public Mock<ISettingsProvider> _mockSettingsProvider;

        public Mock<IDataRepository<TestCreature>> _mockDataRepository;



        [SetUp]
        public void Setup()
        {
            _mockSettingsProvider = new Mock<ISettingsProvider>();

            _mockDataRepository = new Mock<IDataRepository<TestCreature>>();

            _mutator = new TopPercentMutator<TestCreature>(
                _mockSettingsProvider.Object,
                _mockDataRepository.Object,
                new Random());
        }

        [Test]
        public void Mutate_gets_the_top_percent_setting_from_the_settings_provider()
        {
            _mutator.Mutate();

            _mockSettingsProvider
                .Verify(x => x.GetDoubleSetting(TopPercentMutator<TestCreature>.TOP_PERC_SETTING));
        }

        [Test]
        public void Mutate_calls_mutate_on_the_top_creatures()
        {
            var topPercent = 0.2;
            var creatureCount = 20;
            var expectedTaken = (int)(creatureCount * topPercent);
            var expectedNotTaken = creatureCount - expectedTaken;

            var creatures = new List<TestCreature>();
            var scores = new Dictionary<TestCreature, double>();

            for (var i = 0; i < creatureCount; i++)
            {
                var creature = _fixture.Create<TestCreature>();
                creatures.Add(creature);
                scores.Add(creature, creatureCount - i);
            }

            _mockDataRepository
                .Setup(x => x.GetScore(It.IsAny<TestCreature>()))
                .Returns<TestCreature>(creature => scores[creature]);
            _mockDataRepository
                .Setup(x => x.GetCreatures())
                .Returns(creatures);
            _mockSettingsProvider
                .Setup(x => x.NumberOfCreatures)
                .Returns(creatureCount);
            _mockSettingsProvider
                .Setup(x => x.GetDoubleSetting(TopPercentMutator<TestCreature>.TOP_PERC_SETTING))
                .Returns(topPercent);

            _mutator.Mutate();

            for (var i = 0; i < creatureCount; i++)
            {
                if(i < expectedTaken)
                {
                    Assert.True(creatures[i].WasMutated, string.Format(
                        "Creature not mutated. position:{0} cutoff:{1}", i, expectedTaken));
                }
                else
                {
                    Assert.False(creatures[i].WasMutated, string.Format(
                        "Creature unexpectedly mutated. position:{0} cutoff:{1}", i, expectedTaken));
                }
            }
        }

        [Test]
        public void Mutate_always_at_least_calls_mutate_on_the_single_top_creature()
        {
            var topPercent = 0.2;
            var creatureCount = 2;
            
            var creatures = new List<TestCreature>();
            var scores = new Dictionary<TestCreature, double>();

            for (var i = 0; i < creatureCount; i++)
            {
                var creature = _fixture.Create<TestCreature>();
                creatures.Add(creature);
                scores.Add(creature, creatureCount - i);
            }

            _mockDataRepository
                .Setup(x => x.GetScore(It.IsAny<TestCreature>()))
                .Returns<TestCreature>(creature => scores[creature]);
            _mockDataRepository
                .Setup(x => x.GetCreatures())
                .Returns(creatures);
            _mockSettingsProvider
                .Setup(x => x.NumberOfCreatures)
                .Returns(creatureCount);
            _mockSettingsProvider
                .Setup(x => x.GetDoubleSetting(TopPercentMutator<TestCreature>.TOP_PERC_SETTING))
                .Returns(topPercent);

            _mutator.Mutate();

            Assert.True(creatures[0].WasMutated, "No creatures mutated.");
        }

        [Test]
        public void Mutate_muates_and_saves_enough_creatures_to_totally_fill_the_next_generation()
        {
            var topPercent = 0.2;
            var creatureCount = 20;

            var creatures = new List<TestCreature>();
            var scores = new Dictionary<TestCreature, double>();

            for (var i = 0; i < creatureCount; i++)
            {
                var creature = _fixture.Create<TestCreature>();
                creatures.Add(creature);
                scores.Add(creature, creatureCount - i);
            }

            _mockDataRepository
                .Setup(x => x.GetScore(It.IsAny<TestCreature>()))
                .Returns<TestCreature>(creature => scores[creature]);
            _mockDataRepository
                .Setup(x => x.GetCreatures())
                .Returns(creatures);
            _mockSettingsProvider
                .Setup(x => x.NumberOfCreatures)
                .Returns(creatureCount);
            _mockSettingsProvider
                .Setup(x => x.GetDoubleSetting(TopPercentMutator<TestCreature>.TOP_PERC_SETTING))
                .Returns(topPercent);

            _mutator.Mutate();

            _mockDataRepository.Verify(x => x.AddCreatures(
                It.Is<IEnumerable<TestCreature>>(y => y.Count() == creatureCount)));
        }
    }
}
