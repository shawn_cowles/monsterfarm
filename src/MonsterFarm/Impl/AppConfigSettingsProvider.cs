﻿using System;
using System.Configuration;
using System.Linq;
using MonsterFarm.Interfaces;

namespace MonsterFarm.Impl
{
    /// <summary>
    /// An <see cref="ISettingsProvider"/> that pulls settings from the application configuration 
    /// file.
    /// </summary>
    public class AppConfigSettingsProvider : ISettingsProvider
    {
        /// <summary>
        /// The key for the GenerationsToRunSetting.
        /// </summary>
        public const string SETTING_GENERATIONS = "generations_to_run";

        /// <summary>
        /// The key for the NumberOfCreatures setting.
        /// </summary>
        public const string SETTING_NUMBER_CREATURES = "number_of_creatures";

        /// <summary>
        /// The number of generations to run.
        /// </summary>
        public int GenerationsToRun { get; private set; }

        /// <summary>
        /// The number of creatures in each generation.
        /// </summary>
        public int NumberOfCreatures { get; private set; }

        /// <summary>
        /// Construct a new AppConfigSettingsProvider.
        /// </summary>
        public AppConfigSettingsProvider()
        {
            GenerationsToRun = GetIntSetting(SETTING_GENERATIONS);

            NumberOfCreatures = GetIntSetting(SETTING_NUMBER_CREATURES);
        }

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <returns>The string value of the setting, if found, null otherwise.</returns>
        public string GetStringSetting(string settingName)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(settingName))
            {
                throw new SettingNotFoundException(settingName + " not found.");
            }

            return ConfigurationManager.AppSettings[settingName];
        }

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <param name="defaultValue">The default value to return.</param>
        /// <returns>The string value of the setting, if found, <paramref name="defaultValue"/>
        /// otherwise.</returns>
        /// <returns>The int value of the setting, if found, null otherwise.</returns>
        public string GetStringSettingOrDefault(string settingName, string defaultValue)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(settingName))
            {
                return defaultValue;
            }

            return ConfigurationManager.AppSettings[settingName];
        }

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <returns>The int value of the setting, if found, null otherwise.</returns>
        public int GetIntSetting(string settingName)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(settingName))
            {
                throw new SettingNotFoundException(settingName + " not found.");
            }

            return int.Parse(ConfigurationManager.AppSettings[settingName]);
        }

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <param name="defaultValue">The default value to return.</param>
        /// <returns>The int value of the setting, if found, <paramref name="defaultValue"/>
        /// otherwise.</returns>
        public int GetIntSettingOrDefault(string settingName, int defaultValue)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(settingName))
            {
                return defaultValue;
            }

            int val;

            if (int.TryParse(ConfigurationManager.AppSettings[settingName], out val))
            {
                return val;
            }

            return defaultValue;
        }

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <returns>The double value of the setting, if found, null otherwise.</returns>
        public double GetDoubleSetting(string settingName)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(settingName))
            {
                throw new SettingNotFoundException(settingName + " not found.");
            }

            return double.Parse(ConfigurationManager.AppSettings[settingName]);
        }

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <param name="defaultValue">The default value to return.</param>
        /// <returns>The double value of the setting, if found, <paramref name="defaultValue"/>
        /// otherwise.</returns>
        public double GetDoubleSettingOrDefault(string settingName, double defaultValue)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(settingName))
            {
                return defaultValue;
            }

            double val;

            if (double.TryParse(ConfigurationManager.AppSettings[settingName], out val))
            {
                return val;
            }

            return defaultValue;
        }
    }
}
