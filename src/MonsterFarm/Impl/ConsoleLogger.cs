﻿using System;
using MonsterFarm.Interfaces;

namespace MonsterFarm.Impl
{
    /// <summary>
    /// A logger that outputs to the standard console.
    /// </summary>
    public class ConsoleLogger : ILogger
    {
        /// <summary>
        /// Report a status message.
        /// </summary>
        /// <param name="message">The status message to report.</param>
        public void ReportStatus(string message)
        {
            Console.WriteLine(message);
        }

        /// <summary>
        /// Report numerical progress, as a percentage.
        /// </summary>
        /// <param name="progress">The progress percentage, expected to be between 0 and 100
        /// inclusive).</param>
        public void ReportProgress(int progress)
        {
            Console.WriteLine(progress + "%");
        }
    }
}
