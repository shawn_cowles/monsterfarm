﻿using System;
using System.Collections.Generic;
using System.Linq;
using MonsterFarm.Interfaces;
using MonsterFarm.Data;

namespace MonsterFarm.Impl
{
    /// <summary>
    /// A mutator that takes a top percentage of each generation, cloning and mutating them to 
    /// create the next generation.
    /// Percentage to take expected in the TopPercentMutator.TopPercent setting as a double.
    /// </summary>
    /// <typeparam name="T">The type of creature the farm is operating on.</typeparam>
    public class TopPercentMutator<T>: IMutator<T> where T: ICreature
    {
        internal const string TOP_PERC_SETTING = "TopPercentMutator.TopPercent";

        private ISettingsProvider _settingsProvider;

        private IDataRepository<T> _dataRepository;

        private Random _rng;

        /// <summary>
        /// Construct a new TopPercentMutator.
        /// </summary>
        /// <param name="settingsProvider">The setting provider to use.</param>
        /// <param name="dataRepository">The data repository to use.</param>
        /// <param name="rng">The Random to use for random number generation.</param>
        public TopPercentMutator(
            ISettingsProvider settingsProvider,
            IDataRepository<T> dataRepository,
            Random rng)
        {
            _settingsProvider = settingsProvider;
            _dataRepository = dataRepository;
            _rng = rng;
        }

        /// <summary>
        /// Create the next generation of creatures by mutating the current generation.
        /// </summary>
        public void Mutate()
        {
            var topPercent = _settingsProvider.GetDoubleSetting(TOP_PERC_SETTING);
            var numberOfCreatures = _settingsProvider.NumberOfCreatures;

            var numberToTake = Math.Max(1, (int)(topPercent * numberOfCreatures));

            var winners = _dataRepository.GetCreatures()
                .OrderByDescending(x => _dataRepository.GetScore(x))
                .Take(numberToTake)
                .ToArray();

            var nextGeneration = new List<T>();

            for (var i = 0; i < numberOfCreatures; i++)
            {
                var creature = winners[i % winners.Length];

                nextGeneration.Add((T) creature.Mutate(_rng));
            }

            _dataRepository.AddCreatures(nextGeneration);
        }
    }
}
