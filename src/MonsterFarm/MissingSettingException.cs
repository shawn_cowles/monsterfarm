﻿using System;

namespace MonsterFarm
{
    /// <summary>
    /// An exception indicating that a required setting was not found.
    /// </summary>
    public class SettingNotFoundException : Exception
    {
        /// <summary>
        /// Construct a new SettingNotFoundException.
        /// </summary>
        /// <param name="message">A message describing the exception.</param>
        public SettingNotFoundException(string message)
            :base(message)
        {
        }
    }
}
