﻿using System;
using System.Collections.Generic;
using MonsterFarm.Data;
using MonsterFarm.Interfaces;

namespace MonsterFarm
{
    /// <summary>
    /// A monster farm that evolves many generations of a creature, evaluating and mutating each
    /// generation.
    /// </summary>
    /// <typeparam name="T">The type of creature this farm is operating on.</typeparam>
    public class Farm<T> where T : ICreature
    {
        private readonly IDataInitializer<T> _dataInitializer;
        private IDataRepository<T> _dataRepository;
        private IEvaluator<T> _evaluator;
        private ISettingsProvider _settingsProvider;
        private IMutator<T> _mutator;
        private ILogger _logger;

        /// <summary>
        /// Construct a new Farm.
        /// </summary>
        /// <param name="dataInitializer">The data initializer to use.</param>
        /// <param name="dataRepository">The data repository to use.</param>
        /// <param name="evaluator">The evaluator to use.</param>
        /// <param name="settingsProvider">The setting provider to use.</param>
        /// <param name="mutator">The mutator to use.</param>
        /// /<param name="logger">The logger to use to report progress.</param>
        public Farm(
            IDataInitializer<T> dataInitializer,
            IDataRepository<T> dataRepository,
            IEvaluator<T> evaluator,
            ISettingsProvider settingsProvider,
            IMutator<T> mutator,
            ILogger logger
            )
        {
            _dataInitializer = dataInitializer;
            _dataRepository = dataRepository;
            _evaluator = evaluator;
            _settingsProvider = settingsProvider;
            _mutator = mutator;
            _logger = logger;
        }

        /// <summary>
        /// Run the monster farm.
        /// </summary>
        public void Run()
        {
            _logger.ReportStatus("Initializing.");
            _dataInitializer.Initialize();

            _logger.ReportStatus("Running Farm...");
            var numGenerations = _settingsProvider.GenerationsToRun;

            var lastPercentReported = -1;

            for(var currentGeneration = 0 ; currentGeneration < numGenerations; currentGeneration++)
            {
                foreach(var creature in _dataRepository.GetCreatures())
                {
                    var score = _evaluator.EvaluateCreature(creature);
                    _dataRepository.RecordScore(creature, score);
                }

                // Mutate only if there will be a next generation.
                if (currentGeneration < numGenerations - 1)
                {
                    _mutator.Mutate();
                }

                var currentPercent = (int)((currentGeneration / (double)numGenerations) * 100);

                if(currentPercent > lastPercentReported)
                {
                    lastPercentReported = currentPercent;

                    _logger.ReportProgress(currentPercent);
                }
            }

            _logger.ReportStatus("Farm Finished.");
        }
    }
}
