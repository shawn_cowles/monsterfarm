﻿using MonsterFarm.Data;

namespace MonsterFarm.Interfaces
{
    /// <summary>
    /// The evaluator that evaluates each generation of creature.
    /// </summary>
    /// <typeparam name="T">The type of creature the farm is operating on.</typeparam>
    public interface IEvaluator<T> where T : ICreature
    {
        /// <summary>
        /// Evaluate a creature.
        /// </summary>
        /// <param name="creature">The creature to evaluate.</param>
        /// <returns>The "score" of the creature, higher is better.</returns>
        double EvaluateCreature(T creature);
    }
}
