﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MonsterFarm.Interfaces
{
    /// <summary>
    /// An interface for a settings provider for the farm.
    /// </summary>
    public interface ISettingsProvider
    {
        /// <summary>
        /// The number of generations to run.
        /// </summary>
        int GenerationsToRun { get; }

        /// <summary>
        /// The number of creatures in each generation.
        /// </summary>
        int NumberOfCreatures { get; }

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <returns>The string value of the setting, if found, null otherwise.</returns>
        string GetStringSetting(string settingName);

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <param name="defaultValue">The default value to return.</param>
        /// <returns>The string value of the setting, if found, <paramref name="defaultValue"/>
        /// otherwise.</returns>
        string GetStringSettingOrDefault(string settingName, string defaultValue);

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <returns>The int value of the setting, if found, null otherwise.</returns>
        int GetIntSetting(string settingName);

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <param name="defaultValue">The default value to return.</param>
        /// <returns>The int value of the setting, if found, <paramref name="defaultValue"/>
        /// otherwise.</returns>
        int GetIntSettingOrDefault(string settingName, int defaultValue);

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <returns>The double value of the setting, if found, null otherwise.</returns>
        double GetDoubleSetting(string settingName);

        /// <summary>
        /// Get an implementation specific setting.
        /// </summary>
        /// <param name="settingName">The name of the setting.</param>
        /// <param name="defaultValue">The default value to return.</param>
        /// <returns>The double value of the setting, if found, <paramref name="defaultValue"/>
        /// otherwise.</returns>
        double GetDoubleSettingOrDefault(string settingName, double defaultValue);
    }
}
