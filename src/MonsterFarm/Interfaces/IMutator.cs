﻿using MonsterFarm.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MonsterFarm.Interfaces
{
    /// <summary>
    /// An interface for a mutator for the farm. The mutator is responsible for creating the
    /// next generation of creatures based on the current generation.
    /// </summary>
    /// <typeparam name="T">The type of creature the farm is operating on.</typeparam>
    public interface IMutator<T> where T: ICreature
    {
        /// <summary>
        /// Create the next generation of creatures by mutating the current generation.
        /// </summary>
        void Mutate();
    }
}
