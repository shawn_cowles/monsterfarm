﻿using System.Collections.Generic;
using MonsterFarm.Data;

namespace MonsterFarm.Interfaces
{
    /// <summary>
    /// Stores information about the creatures and results of evaluations.
    /// </summary>
    /// <typeparam name="T">The type of creature the farm is operating on.</typeparam>
    public interface IDataRepository<T> where T : ICreature
    {
        /// <summary>
        /// Get a specific generation of creatures.
        /// </summary>
        /// <param name="generation">The generation of creatures to return.</param>
        /// <returns>The specified generation of creatures.</returns>
        IEnumerable<T> GetCreatures(int generation);

        /// <summary>
        /// Get the most recent generation of creatures.
        /// </summary>
        /// <returns>The most recent generation of creatures.</returns>
        IEnumerable<T> GetCreatures();

        /// <summary>
        /// Store a set of creatures as a new generation.
        /// </summary>
        /// <param name="creatures">The creatures to store.</param>
        void AddCreatures(IEnumerable<T> creatures);

        /// <summary>
        /// Record an evaluation score for a creature.
        /// </summary>
        /// <param name="creature">The creature.</param>
        /// <param name="score">The evaluation score for the creature.</param>
        void RecordScore(T creature, double score);

        /// <summary>
        /// Return the stored evaluation score for a creature.
        /// </summary>
        /// <param name="creature">The creature.</param>
        /// <returns>The stored evaluation score for <paramref name="creature"/>, or null if
        /// there is no stored score for the specified creature.</returns>
        double? GetScore(T creature);

        /// <summary>
        /// Get the average score for the latest generation of creatures.
        /// </summary>
        /// <returns>The average score for the latest generation of creatures.</returns>
        double GetAverageScore();

        /// <summary>
        /// Get the average score for the specified generation of creatures.
        /// </summary>
        /// <param name="generation">The specific generation of scores to retrieve.</param>
        /// <returns>The average score for the specified generation of creatures, or null if
        /// there are no recorded scores for the specified generation.</returns>
        double? GetAverageScore(int generation);
    }
}
