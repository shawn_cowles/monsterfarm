﻿namespace MonsterFarm.Interfaces
{
    /// <summary>
    /// Logs information about the current Farm run.
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Report a status message.
        /// </summary>
        /// <param name="message">The status message to report.</param>
        void ReportStatus(string message);

        /// <summary>
        /// Report numerical progress, as a percentage.
        /// </summary>
        /// <param name="progress">The progress percentage, expected to be between 0 and 100
        /// inclusive).</param>
        void ReportProgress(int progress);
    }
}
