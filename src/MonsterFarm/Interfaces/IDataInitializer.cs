﻿using MonsterFarm.Data;
namespace MonsterFarm.Interfaces
{
    /// <summary>
    /// An interface for the DataInitializer. The DataInitializer is responsible for setting
    /// up the starting state of the farm for generation, this typically includes creating
    /// the first generation of creatures.
    /// </summary>
    /// <typeparam name="T">The type of creature the farm is operating on.</typeparam>
    public interface IDataInitializer<T> where T : ICreature
    {
        /// <summary>
        /// Initialize the data repository.
        /// </summary>
        void Initialize();
    }
}
