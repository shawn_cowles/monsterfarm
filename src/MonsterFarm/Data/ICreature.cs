﻿using System;

namespace MonsterFarm.Data
{
    /// <summary>
    /// The subject of the monster farm, the "creature" that is being evolved.
    /// </summary>
    public interface ICreature
    {
        /// <summary>
        /// A unique ID for this creature.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Clone and mutate the creature.
        /// </summary>
        /// <param name="rnd">A random number generator to use in mutation.</param>
        /// <returns>A mutated clone of this creature.</returns>
        ICreature Mutate(Random rnd);
    }
}
